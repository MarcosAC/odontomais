        <footer id="rodape">
            <div class="container">
              <div class="row">
                <div class="col-md-4">
                  <img src="imagens/img-5.jpg" class="img-responsive">
                  <div id="rodape-endereco">              
                    <p><span class="glyphicon glyphicon-map-marker"></span> Rua Padre Serafim n°108 Loja 2 - Viçosa MG</p>
                    <p><span class="glyphicon glyphicon-phone"></span> (31) 3892 6170</p>
                    <p><span class="glyphicon glyphicon-phone"></span> (31) 9 8517 7616</p>
                    <p><span class="glyphicon glyphicon-envelope"></span> odontomaisvicosa@gmail.com</p>         
                  </div>
                </div>
            
                <div id="rodape-pagamento" class="col-md-4">
                  <h3>Formas de Pagamento:</h3>
                  <div id=forma-pagamento>
                    <i class="fa fa-cc-amex fa-3x" aria-hidden="true"></i>
                    <i class="fa fa-cc-diners-club fa-3x" aria-hidden="true"></i>
                    <i class="fa fa-cc-mastercard fa-3x" aria-hidden="true"></i>
                    <i class="fa fa-cc-visa fa-3x" aria-hidden="true"></i>
                    <i class="fa fa-credit-card fa-3x" aria-hidden="true"></i>
                  </div>
                  <p>Parcelamos em até 12x no cartão de credito e debito.</p>
                </div>
            
                <div class="col-md-4">            
                  <ul>
                    <li class="item-rede-social"><a href="https://www.facebook.com/odontomaisvicosa/" target="_blank"><i class="fa fa-facebook-official fa-4x" aria-hidden="true"></i></a></li>
                    <li class="item-rede-social"><a href=""><i class="fa fa-instagram fa-4x" aria-hidden="true"></i></a></li>              
                    <li class="item-rede-social"><a href=""><i class="fa fa-whatsapp fa-4x" aria-hidden="true"></i></a></li>
                    <li class="item-rede-social"><a href="contato.php"><i class="fa fa-envelope-o fa-4x" aria-hidden="true"></i></a></li>
                  </ul>            
                </div>
              </div>
            
              <div class="row" id="rodape-copyright">          
                  <a href="http://marcosaurelio.azurewebsites.net/" target="_blank">Marcos Aurelio</a> - © Todos os Direitos Reservados 2017                        
              </div>        
            </div>         
        </footer>
    
        <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
        <script src="js/bootstrap.min.js"></script>
  </body>
</html>