<?php include('header.php'); ?>

<?php

  require_once('classes/ConexaoBD.php');
  
  $BancoDados = new ConexaoBD();
  $stringConexao = $BancoDados->Conexao();  
  
   //Verificar se está sendo passado na URL a página atual, se nâo é atribuido a pagina 
   $pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1;  
   $id = $_GET['id'];   
   $categoriaAtual = 'id='.$id;
   
   //Selecionar todos os produtos da subcategoria.
   $strSQL = "Select * From produto Where IdSubCategoria = $id";
   $todosProdutos = mysqli_query($stringConexao, $strSQL);
   //Contar o total de produtos
   $totalProdutos = mysqli_num_rows($todosProdutos);
   //Seta a quantidade de produtos por pagina
   $quantidadePaginas = 6;
   //calcular o número de pagina necessárias para apresentar os produtos
   $numeroPaginas = ceil($totalProdutos/$quantidadePaginas);
   //Calcular o inicio da visualizacao
   $inicio = ($quantidadePaginas*$pagina)-$quantidadePaginas;
   //Selecionar os produtos a serem apresentado na página
   $SqlComando = "Select * From produto Where IdSubCategoria = $id Limit $inicio, $quantidadePaginas";  
   $listaProdutoCategoria = mysqli_query($stringConexao, $SqlComando);
   $totalProdutos = mysqli_num_rows($listaProdutoCategoria);
   $SqlSubcategoria = "Select * From subcategoria Where IdSubCategoria = $id";
   $listaSubcategoria = mysqli_query($stringConexao, $SqlSubcategoria);
   $totalSubcategoria = mysqli_num_rows($listaSubcategoria);
   $subcategoria = mysqli_fetch_array($listaSubcategoria, MYSQLI_ASSOC);   


?>

<div class="container" id="produtos">  
  <h2><?= $subcategoria['SubCategoria']; ?></h2>     
  <div class="row">
    <?php if (empty($totalProdutos)) { ?>
      <h2>Não existe produtos para essa categoria.</h2>
    <?php } else { ?>
      <?php while ($listaProduto = mysqli_fetch_array($listaProdutoCategoria, MYSQLI_ASSOC)) { ?>            
        <div class="col-md-6 col-md-4">
          <div class="thumbnail">
            <img src="imagens/<?= $listaProduto['ImagemProduto']; ?>" class="img-responsive" style="width:242px; height:200px;" alt="Image">
            <hr>
            <div class="caption">
              <p><?= $listaProduto['NomeProduto']; ?></p>
              <h3>R$<?= $listaProduto['Preco']; ?></h3>
              <!-- <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p> -->              
            </div>            
          </div>          
        </div> 
      <?php } ?>
     <?php } ?>
  </div>
  
  <nav class="text-center" aria-label="Page navigation">
    <ul class="pagination">
     
        <li><a href="?<?= $categoriaAtual ?>&pagina=1" aria-label="Previous">
             <span aria-hidden="true">&laquo;</span>
           </a>         
        </li>

      <?php
        //Apresentar a paginacao
        for($i = 1; $i <= $numeroPaginas; $i++) { ?>
          <li>
             <a href="produtos.php?<?= $categoriaAtual ?>&pagina=<?= $i ?>"><?= $i ?></a>
          </li>
      <?php } ?>
        <li>
          <a href="?<?= $categoriaAtual ?>&pagina=<?= $numeroPaginas ?>" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>          
        </li>     
    </ul>
  </nav>
</div> 

<?php include('footer.php'); ?>