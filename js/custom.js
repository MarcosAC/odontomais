//Carrega a imagem selecionada.
var inputFile = document.getElementById('imagem');
var imagemProduto = document.getElementById('imagemProduto');

if (inputFile != null && inputFile.addEventListener)
{                   
  inputFile.addEventListener('change', function(){loadFoto(this, imagemProduto)});
}
else if (inputFile != null && inputFile.attachEvent)
{                  
  inputFile.attachEvent('onchange', function(){loadFoto(this, imagemProduto)});
}       