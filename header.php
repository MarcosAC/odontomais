<?php
   require_once('classes/ConexaoBD.php');
  
    $BancoDados = new ConexaoBD();
    $stringConexao = $BancoDados->Conexao();
  
    //"Select s.IdSubCategoria, s.SubCategoria, c.Categoria From subcategoria as s Inner Join categoria as c On s.IdCategoria = c.IdCategoria"
    //$sqlSubCategoria = "Select IdSubCategoria, SubCategoria From subcategoria Where IdCategoria = $id";
    //$carregaSubCategoria = mysqli_query($stringConexao, $sqlSubCategoria); 

    $sqlCategoria = "Select * From categoria";
    $carregaCategoria = mysqli_query($stringConexao, $sqlCategoria);    
    //$listaCategoria = mysqli_num_rows($carregaCategoria);
  
?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Odonto Mais</title>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
    <!-- <script src="js/bootstrap-dropdownhover.min.js"></script> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">    
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <!-- <link href="css/animate.min.css" rel="stylesheet"> -->
    <!-- <link href="css/bootstrap-dropdownhover.min.css" rel="stylesheet">  -->

    <!-- HTML5 shim e Respond.js para suporte no IE8 de elementos HTML5 e media queries -->
    <!-- ALERTA: Respond.js não funciona se você visualizar uma página file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  <!--<script type='text/javascript'>
    function init_map()
      {
        var myOptions = {zoom:16, center:new google.maps.LatLng(-20.7555864,-42.87842639999997), mapTypeId: google.maps.MapTypeId.ROADMAP}
            map = new google.maps.Map(document.getElementById('gmap_canvas'),myOptions);
            marker = new google.maps.Marker({map: map, position: new google.maps.LatLng(-20.7555864,-42.87842639999997)});
            infowindow = new google.maps.InfoWindow({content:'<strong>Odonto Mais</strong><br>Rua: Padre Serafim n°108 Loja 2 - Viçosa MG<br>'});
            google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});
            infowindow.open(map,marker);
      }
    google.maps.event.addDomListener(window, 'load', init_map);
  </script>-->

  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">          

          <!-- Botão Toggle -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#barra-navegacao">
            <span class="sr-only">Alternar navegacao</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <a href="index.php" class="navbar-brand"><img class="logo" src="imagens/logo.jpg"></a> 
        </div> 

        <!-- Barra de Navegação -->               
        <div class="collapse navbar-collapse" id="barra-navegacao">
          <ul class="nav navbar-nav">            
            <li class="dropdown"> <!-- Inicio Dropdown -->
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">Categorias<span class="caret"></span></a>              
              <ul class="dropdown-menu">
                <?php while ($listaCategoria = mysqli_fetch_array($carregaCategoria, MYSQLI_ASSOC)) { ?>                
                  <li class="dropdown-submenu">                  
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"><?= $listaCategoria['Categoria']; ?></a>
                    <?php $id = $listaCategoria['IdCategoria']; ?>                  
                    <ul class="dropdown-menu">
                      <?php
                        $sqlSubCategoria = "Select IdSubCategoria, SubCategoria From subcategoria Where IdCategoria = $id;";
                        $carregaSubCategoria = mysqli_query($stringConexao, $sqlSubCategoria);
                      ?>                      
                      <?php while ($listaSubCategoria = mysqli_fetch_array($carregaSubCategoria, MYSQLI_ASSOC)) { ?>
                        <li><a href="produtos.php?id=<?= $listaSubCategoria['IdSubCategoria']; ?>"><?= $listaSubCategoria['SubCategoria']; ?></a></li>                        
                      <?php } ?>                      
                    </ul>
                  </li>
                <?php } ?>
              </ul>              
            </li>
            <li><a href="institucional.php">Institucional</a></li>
            <li><a href="contato.php">Contato</a></li>          
          </ul>
          <!--<form class="navbar-form navbar-right" method="get" action="pesquisa.php"> 
            <div class="form-group">
              <input type="text" class="form-control" name="pesquisa" placeholder="Procurar produto">
            </div>
            <a type="submit" class="btn btn-default" href="produtos.php?condicao=<?= $condicao; ?>">
              <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
            </a>
          </form>-->
        </div> <!-- Fim Barra de Navegação -->

      </div>         
    </nav>