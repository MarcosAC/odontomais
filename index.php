<?php include('header.php'); ?>

<?php

  require_once('classes/ConexaoBD.php');
  
  $BancoDados = new ConexaoBD();
  $stringConexao = $BancoDados->Conexao();

  $strSQL = "Select * From produto Where IdSubCategoria = 85";
  $produtoPromocao = mysqli_query($stringConexao, $strSQL);

?>

<section id="banner">
  <div class="container"> <!-- Inicio Banner -->      
   <div id="carousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carousel" data-slide-to="0" class="active"></li>
      <li data-target="#carousel" data-slide-to="1"></li>
      <li data-target="#carousel" data-slide-to="2"></li>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="imagens/img-1.png" alt="Imagem1">
        <div class="carousel-caption">
          Imagem1
        </div>
      </div>
      <div class="item">
        <img src="imagens/img-2.png" alt="Imagem2">
        <div class="carousel-caption">
          Imagem2
        </div>
      </div>
      <div class="item">
        <img src="imagens/img-3.png" alt="Imagem3">
        <div class="carousel-caption">
          Imagem3
        </div>
      </div>
    </div>
     <!-- Controls -->
     <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
       <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
       <span class="sr-only">Previous</span>
     </a>
     <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
       <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
       <span class="sr-only">Next</span>
     </a>
   </div>     
  </div> <!-- Fim Banner -->      
</section>

<div class="container" id="produtos">
  <h3>Promoções</h3>
  <div class="row">
    <?php while($listaProdutoPromocao = mysqli_fetch_array($produtoPromocao)) {?>
      <div class="col-md-6 col-md-4">
        <div class="thumbnail">
          <img src="imagens/<?= $listaProdutoPromocao['ImagemProduto']; ?>" class="img-responsive" style="width:242px; height:200px;" alt="Image">
          <hr>
          <div class="caption">            
            <p><?= $listaProdutoPromocao['NomeProduto']; ?></p>
            <h3>R$<?= $listaProdutoPromocao['Preco']; ?></h3>
            <!-- <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p> -->              
          </div>            
        </div>
      </div>
    <?php } ?>
  </div>
</div>

<?php include('footer.php'); ?>