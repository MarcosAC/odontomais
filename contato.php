<?php include('header.php'); ?>  

  <div id="contato" class="container">
    <h2>Contato</h2>

    <form>
      <div class="row"> <!-- inicio pagina -->
        <div class="col-md-12"> <!-- inicio grid principal -->
          <div class="col-md-1"></div>

          <div id="endereco" class="col-md-4">
              <p><span class="glyphicon glyphicon-map-marker"></span> Rua Padre Serafim n°108 Loja 2 - Viçosa MG</p>
              <p><span class="glyphicon glyphicon-map-marker"></span><a href="https://www.google.com.br/maps/place/R.+Padre+Serafim,+108+-+Bela+Vista,+Vi%C3%A7osa+-+MG,+36570-000/@-20.7557159,-42.8806066,17z/data=!3m1!4b1!4m5!3m4!1s0xa367e92cac4bf3:0xd085a15f6a1691bc!8m2!3d-20.7557209!4d-42.8784179" target="_blank" style="color: #27408B;"> Como Chegar</a></p>             
              <p><span class="glyphicon glyphicon-phone"></span> (31) 3892 6170</p>
              <p><span class="glyphicon glyphicon-phone"></span> (31) 9 8517 7616</p>
              <p><span class="glyphicon glyphicon-envelope"></span> odontomaisvicosa@gmail.com</p>               
          </div>
          
          <div class="col-md-6"> <!-- inicio grid formulário -->
            <div class="row">
              
              <div class="form-group">
                <!-- <label>Nome</label> -->
                <input type="text" name="nome" class="form-control" placeholder="Nome">
              </div>

              <div class="form-group">
                <!-- <label>E-mail</label> -->
                <input type="text" name="email" class="form-control" placeholder="E-mail">
              </div>

              <div class="form-group">
                <!-- <label>Telefone</label> -->
                <input type="text" name="telefone" class="form-control" placeholder="Telefone">
              </div>

              
              <div class="form-group">
                <!-- <label>Mensagem</label> -->
                <textarea name="mensagem" class="form-control" rows="5" placeholder="Mensagem"></textarea>
              </div>

             <div class="form-group">
                <button type="submit" name="enviar" class="btn btn-default pull-right" value="Enviar" id="btnEnviar">Enviar</button>
             </div>              

            </div>
          </div><!-- fim grid formulário -->

          <div class="col-md-1"></div>
        </div><!-- inicio grid principal -->
      </div> <!-- inicio pagina -->
    </form>
  </div>  

<?php include('footer.php'); ?>