<?php

    session_start();

    require_once('ConexaoBD.php');

    $idProduto = $_POST['id'];
    $produto = $_POST['produto'];    
    $imagem = $_POST['imagemAtual'];    
    $preco = $_POST['preco'];

    $BancoDados = new ConexaoBD();
    $stringConexao = $BancoDados->Conexao();
    
    if (isset($_FILES['imagem']) && $_FILES['imagem']['size'] > 0)
    {
        // Verifica se a foto é diferente da padrão, se verdadeiro exclui a foto antiga da pasta
        if ($imagem <> 'padrao.png')
        {
            unlink("../imagens/" . $imagem);
        }

        // Verifica se o upload foi enviado via POST
        if (is_uploaded_file($_FILES['imagem']['tmp_name']))
        {
            // Pega extensão da imagem
	        preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $_FILES['imagem']["name"], $ext);
    
            // Gera um nome único para a imagem
            $imagem = md5(uniqid(time())) . "." . $ext[1];
    
            // Caminho de onde ficará a imagem
            $localImagem = "../imagens/" . $imagem;
    
            // Faz o upload da imagem para seu respectivo caminho
            move_uploaded_file($_FILES['imagem']["tmp_name"], $localImagem);
        }   
    }    
    else
    {
        $imagem = $imagem;
    }

    $comandoSQL = "Update produto Set NomeProduto='$produto', ImagemProduto='$imagem', Preco='$preco' Where IdProduto='$idProduto'";  
    
    if (mysqli_query($stringConexao, $comandoSQL))
    {
        echo 'Produto atualizado com sucesso!';
    }
    else
    {
        echo 'Erro ao atualizar o produto! '. mysqli_error($stringConexao);
    }
   
?>