<?php
	
	class ConexaoBD
	{
		
		private $Host = 'localhost';
		private $Usuario = 'root';
		private $Senha = '';
		private $BancoDados = 'odontomaisdb';

		//Função de conexão com o banco de dados.
		public function Conexao()
		{
			//Cria a conexão com o banco de dodos.
			$strConexao = mysqli_connect($this->Host, $this->Usuario, $this->Senha, $this->BancoDados);

			//ajustar o charset de comunicação entre a aplicação e o banco de dados.
			mysqli_set_charset($strConexao, 'utf8');

			if(mysqli_connect_errno())
			{
				echo 'Erro ao conectar com o Banco de Dados: '.mysqli_connect_error();
			}

			return $strConexao;
		}
	}

?>