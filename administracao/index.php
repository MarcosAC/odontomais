<!DOCTYPE html>

<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- As 3 meta tags acima *devem* vir em primeiro lugar dentro do `head`; qualquer outro conteúdo deve vir *após* essas tags -->
    <title>Administração - Odonto Mais</title>

    <!-- Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>   
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
    <!-- <script src="js/bootstrap-dropdownhover.min.js"></script> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">    
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/estilo.css" rel="stylesheet">

    <!-- HTML5 shim e Respond.js para suporte no IE8 de elementos HTML5 e media queries -->
    <!-- ALERTA: Respond.js não funciona se você visualizar uma página file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <h1 style="text-align: center; color: #27408B;">Administração Odonto Mais</h1>

      <div class="col-sm-12">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
          <div class="panel panel-primary">
            <div class="panel-heading">Login Usuário</div>
            <div class="panel-body">
              <form method="post" action="/odontomais/classes/ValidarAcesso.php" class="form-horizontal">
          
              <div class="form-group">
                <label class="col-sm-2 control-label">Nome</label>
                <div class="col-sm-10">
                  <input type="text" name="nomeUsuario" class="form-control" id="nomeUsuario" placeholder="Nome">
                </div>        
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Senha</label>
                <div class="col-sm-10">
                  <input type="password" name="senha" class="form-control" id="senha" placeholder="Senha">
                </div>        
              </div>

              <div class="form-group">
                <div class="col-sm-offset-2 control-label col-sm-10">
                  <button type="submit" name="entrar" class="btn btn-primary" id="btnEntrar">Entrar</button>
                </div>                        
              </div>

            </form>
          </div>
          
        </div>
        <div class="col-sm-3"></div>
      </div>
    </div>    

    <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>