<?php

  session_start();  

  if (!isset($_SESSION['tipoUsuario']))
  {
    header('Location: ../administracao/index.php');
  }

  require_once('../classes/ConexaoBD.php');
  
  $BancoDados = new ConexaoBD();
  $stringConexao = $BancoDados->Conexao();
  $comandoSQL = " Select IdSubCategoria, SubCategoria From subcategoria ";
  $carregaSubCategoria = mysqli_query($stringConexao, $comandoSQL);	
  
?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- As 3 meta tags acima *devem* vir em primeiro lugar dentro do `head`; qualquer outro conteúdo deve vir *após* essas tags -->
    <title>Cadastrar Produto - Odonto Mais</title>

    <!-- Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>   
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!--<script src="js/custom.js"></script>-->
    <!-- <script src="js/bootstrap-dropdownhover.min.js"></script> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">    
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/estilo.css" rel="stylesheet">    

    <!-- HTML5 shim e Respond.js para suporte no IE8 de elementos HTML5 e media queries -->
    <!-- ALERTA: Respond.js não funciona se você visualizar uma página file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="menu.php" class="navbar-brand" id="brand-administracao">Odonto Mais - Administração</a>
        </div>

        <div class="navbar-collapse collapse"  id="barra-navegacao-administracao">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="menu.php">Menu Principal</a></li>            
            <li><a href="sair.php">Sair</a></li>
            <li><a href="#">Perfil</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container formularios">
      <h1 class="page-header" style="text-align: center; color: #27408B;">Cadastrar Produto</h1>

      <div class="col-sm-12"><!-- Inicio coluna 12 -->
        <div class="col-sm-1"></div>

        <div class="col-sm-10"><!-- Inicio coluna 10 -->
          <div class="panel panel-primary">
            <div class="panel-heading">Dados do Produto</div>
            <div class="panel-body">
              <form method="post" action="/odontomais/classes/InserirProduto.php" id="FormEditarProduto" enctype="multipart/form-data"><!-- Inicio formulário -->

                <div class="row"><!-- Inicio row -->                  
                  <div class="col-sm-7"><!-- Inicio coluna 7 -->
                    <div class="form-group">                      
                      <label>Nome produto</label>
                      <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></div>
                        <input type="text" name="produto"  class="form-control" placeholder="Produto">
                      </div>                
                    </div>

                    <div class="row"><!-- Inicio row -->
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Categoria</label>       
                          <select name="idSubCategoria" class="form-control">                          
                            <?php while ($subCategoria = mysqli_fetch_array($carregaSubCategoria)) { ?>
                              <option value="<?= $subCategoria['IdSubCategoria']; ?>">
                                <?= $subCategoria['SubCategoria']; ?>
                              </option>
                            <?php } ?>                            
                          </select>
                        </div>
                      </div>
                        
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Preço do produto</label>
                          <div class="input-group">
                            <div class="input-group-addon">R$</div>                
                              <input type="text" name="preco" class="form-control" placeholder="Preço">
                            </div>
                          </div>  
                        </div>
                      </div>

                      <div class="form-group">
                        <label>Inserir Imagem</label>
                        <input type="file" name="imagem" value="imagem" id="imagem">
                      </div>

                      <div class="form-group">
                        <button type="submit" name="cadastrarProduto" class="btn btn-primary" id="btnCadastrarProduto">Salvar</button>
                        <a class="btn btn-default" href="sair.php">Cancelar</a>
                      </div>
                    </div><!-- Fim row -->                    

                    <div class="col-sm-5">
                      <div class="thumbnail">
                        <img src="../imagens/padrao.png" class="img-responsive" id="imagemProduto"/>                                           
                      </div>                    
                    </div>

                  </div><!-- Fin coluna 7 -->
                </div><!-- Fim row -->                

              </form><!-- Fim formulário -->
            </div>
          </div>
        </div><!-- Fim coluna 10 -->
      </div><!-- Inicio coluna 12 -->       
      <div class="col-sm-1"></div>
    </div>    
    <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
    <script src="js/bootstrap.min.js"></script>    
  </body>
</html>