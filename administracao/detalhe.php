<?php

  session_start();
  
  require_once('../classes/ConexaoBD.php');

  $BancoDados = new ConexaoBD();
  $stringConexao = $BancoDados->Conexao();

  $id = isset($_GET['id']) ? $_GET['id'] : '';

  if ($id == '')
  {
    $comandoSQL = " Select NomeProduto, ImagemProduto, Preco From produto ";
  }
  else
  {
    $comandoSQL = " Select NomeProduto, ImagemProduto, Preco From produto Where IdProduto = ". $id;
  }  

  $resultadoProdutos = mysqli_query($stringConexao, $comandoSQL);

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- As 3 meta tags acima *devem* vir em primeiro lugar dentro do `head`; qualquer outro conteúdo deve vir *após* essas tags -->
    <title>Detalhes Itens - Odonto Mais</title>

    <!-- Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>   
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
    <!-- <script src="js/bootstrap-dropdownhover.min.js"></script> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">    
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/estilo.css" rel="stylesheet">

    <!-- HTML5 shim e Respond.js para suporte no IE8 de elementos HTML5 e media queries -->
    <!-- ALERTA: Respond.js não funciona se você visualizar uma página file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="#" class="navbar-brand" id="brand-administracao">Odonto Mais - Administração</a>
        </div>

        <div class="navbar-collapse collapse"  id="barra-navegacao-administracao">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Menu Principal</a></li>
            <li><a href="#">Perfil</a></li>
            <li><a href="#">Sair</a></li>            
          </ul>
        </div>
      </div>
    </nav>

    <div class="container formularios">
      <?php while ($produtos = mysqli_fetch_object($resultadoProdutos)) {?>
        <h3 class="page-header">Visualizar Item: <?php echo $produtos->NomeProduto; ?></h3>        

        <div class="col-md-12">
          <dir class="col-md-4">
            <div class="row">
              <div class="col-md-4">
                <p><strong>Produto</strong></p>
                <p><?php echo $produtos->NomeProduto; ?></p>            
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <p><strong>Valor</strong></p>
                <p><?php echo $produtos->Preco; ?></p>
              </div>
            </div>            
          </dir>          

          <div class="col-md-4">
            <div>
              <?php echo '<img src="../imagens/'.$produtos->ImagemProduto.'" class="img-responsive"/>' ?> 
            </div>           
          </div>
        </div>         
      <?php } ?>

      <div class="row">
        <div class="col-md-12">
          <hr />
          <a href="" class="btn btn-primary">Voltar</a>
          <a href="" class="btn btn-primary">Editar</a>
          <a href="" class="btn btn-primary">Excluir</a>
        </div>
      </div>

    </div>  
    <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>