<?php

  session_start();

  if (!isset($_SESSION['tipoUsuario']))
  {
    header('Location: ../administracao/index.php');
  }

	require_once('../classes/ConexaoBD.php');

	$BancoDados = new ConexaoBD();
	$stringConexao = $BancoDados->Conexao();

	$comandoSQL = " Select IdTipoUsuario, TipoUsuario From tipousuario ";

	$carregaTipoUsuario = mysqli_query($stringConexao, $comandoSQL);	

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- As 3 meta tags acima *devem* vir em primeiro lugar dentro do `head`; qualquer outro conteúdo deve vir *após* essas tags -->
    <title>Cadastrar Usuário - Odonto Mais</title>

    <!-- Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>   
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
    <!-- <script src="js/bootstrap-dropdownhover.min.js"></script> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">    
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/estilo.css" rel="stylesheet">

    <!-- HTML5 shim e Respond.js para suporte no IE8 de elementos HTML5 e media queries -->
    <!-- ALERTA: Respond.js não funciona se você visualizar uma página file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"    aria-controls="navbar">   
            <span class="sr-only"> Toggle navigation</span>   
            <span class="icon-bar"></span>   
            <span class="icon-bar"></span>   
            <span class="icon-bar"></span>   
          </button>   
          <a href="   #" class="navbar-brand" id="brand-administracao">Odonto Mais - Administração</a>  
        </div>    
    
        <div class="navbar-collapse collapse"  id="barra-navegacao-administracao">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="menu.php">Menu Principal</a></li>            
            <li><a href="sair.php">Sair</a></li>
            <li><a href="#">Perfil</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container formularios">
      <h1 class="page-header" style="text-align: center; color: #27408B;">Administração Odonto Mais</h1>

      <div class="col-sm-12">
        <div class="col-sm-2"></div>

        <div class="col-sm-8">
          <div class="panel panel-primary">
            <div class="panel-heading">Cadastrar Usuário</div>
            <div class="panel-body">
              <form class="form-horizontal" method="post" action="/odontomais/classes/InserirUsuario.php" id="formCadastrarUsuario">

                <div class="form-group">
                  <label class="col-sm-2 control-label">Nome</label>
                  <div class="col-sm-10">
                    <input type="text" name="nome" class="form-control" id="nomeUsuario" placeholder="Nome" required="required">
                    </div>        
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Senha</label>
                  <div class="col-sm-10">
                    <input type="password" name="senha" class="form-control" placeholder="Senha" required="required">
                  </div>        
                </div> 

                <div class="form-group">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-3">
                    <select name="idTipoUsuario" class="form-control">
                      <?php while ($tipoUsuario = mysqli_fetch_array($carregaTipoUsuario)) { ?>                     
                        <option value="<?php echo $tipoUsuario['IdTipoUsuario'] ?>">
                          <?php echo $tipoUsuario['TipoUsuario'] ?>
                        </option>             
                      <?php } ?>                                                    
                    </select>
                  </div>                  
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="entrar" class="btn btn-primary" id="btnEntrar">Cadastar</button>
                    <a class="btn btn-default" href="sair.php">Cancelar</a>
                  </div>                        
                </div>

              </form>
            </div>
          </div>
        </div>

        <div class="col-sm-2"></div>
        <div class="clearfix"></div>
      </div>
    </div>

    <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>