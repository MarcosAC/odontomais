<?php

  session_start();
  
  require_once('../classes/ConexaoBD.php');
  //require_once('../classes/deletar_produto.php');

  $BancoDados = new ConexaoBD();
  $stringConexao = $BancoDados->Conexao();

  $comandoSQL = " Select * From produto ";

  $resultadoProdutos = mysqli_query($stringConexao, $comandoSQL);

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- As 3 meta tags acima *devem* vir em primeiro lugar dentro do `head`; qualquer outro conteúdo deve vir *após* essas tags -->
    <title>Lista - Odonto Mais</title>

    <!-- Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>   
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>     
    <!-- <script src="js/bootstrap-dropdownhover.min.js"></script> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">    
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/estilo.css" rel="stylesheet">    

    <!-- HTML5 shim e Respond.js para suporte no IE8 de elementos HTML5 e media queries -->
    <!-- ALERTA: Respond.js não funciona se você visualizar uma página file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="#" class="navbar-brand" id="brand-administracao">Odonto Mais - Administração</a>
        </div>

        <div class="navbar-collapse collapse"  id="barra-navegacao-administracao">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Menu Principal</a></li>
            <li><a href="#">Perfil</a></li>
            <li><a href="#">Sair</a></li>            
          </ul>
        </div>
      </div>
    </nav>

    <div class="container formularios">    
      <div class="row">
        <div class="col-sm-3">
          <h2>Items</h2>
        </div>

        <div class="col-sm-6">
          <div class="input-group h2">
            <input type="text" name="pesquisar" class="form-control" placeholder="Pesquisar">
            <span class="input-group-btn">
              <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
            </span>
          </div>
        </div>

        <div class="col-sm-3">
          <a href="" class="btn btn-primary pull-right h2">Novo Item</a>
        </div>
        
      </div>

      <hr />

      <div class="row">
        <div class="table-responsive col-md-12">
          <table class="table table-striped" cellspacing="0" cellpadding="0">

            <thead>
              <tr>
                <th>Código</th>
                <th>Nome do Produto</th>
                <th>Preço</th>
                <th>Data do Cadastro</th>
                <th class="actions">Açoes</th>
              </tr>              
            </thead>

            <tbody>
            <?php while ($produtos = mysqli_fetch_array($resultadoProdutos)) {?>
              <tr>
                <td><?php echo $produtos['IdProduto']; ?></td>
                <td><?php echo $produtos['NomeProduto']; ?></td>
                <td><?php echo $produtos['Preco']; ?></td>
                <td><?php echo $produtos['DataCadastroProduto']; ?></td>
                <td class="actions">
                  <a href="detalhe.php?id=<?php echo $produtos['IdProduto']; ?>" class="btn btn-success btn-xs">Visualizar</a>
                  <a href="editar_produto.php?id=<?php echo $produtos['IdProduto']; ?>" class="btn btn-warning btn-xs">Editar</a>
                  <a href="../classes/DeletarProduto.php?id=<?php echo $produtos['IdProduto']; ?>" class="btn btn-danger btn-xs">Excluir</a>
                </td>
              </tr>
            <?php } ?>   
      </div> <!-- Fim lista -->

      <div class="row">
        <div class="col-md-12">
          <ul class="pagination">
              <li class="disable"><a>Anterior</a></li>
              <li class="disable"><a>1</a></li>
              <li><a href="">2</a></li>
              <li><a href="">3</a></li>
              <li class="next"><a href="" rel="next">Próximo</a></li>
          </ul>
        </div>
      </div>

    </div>  
    <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>