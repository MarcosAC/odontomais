<?php
  
  session_start();

  if (!isset($_SESSION['tipoUsuario']))
  {
    header('Location: ../administracao/index.php');
  }

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- As 3 meta tags acima *devem* vir em primeiro lugar dentro do `head`; qualquer outro conteúdo deve vir *após* essas tags -->
    <title>Administração Odonto Mais</title>

    <!-- Bootstrap -->
    <!-- Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>   
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
    <!-- <script src="js/bootstrap-dropdownhover.min.js"></script> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">    
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/estilo.css" rel="stylesheet">

    <!-- HTML5 shim e Respond.js para suporte no IE8 de elementos HTML5 e media queries -->
    <!-- ALERTA: Respond.js não funciona se você visualizar uma página file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <h1 style="text-align: center; color: #27408B;">Administração Odonto Mais</h1>
      <div class="col-sm-12">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
          <div class="panel panel-primary">
            <div class="panel-heading">Menu</div>
            <div class="panel-body">
              <div id="menu-administracao">
                <div class="col-sm-12" style="margin-bottom: 10px;">
                  <a class="btn btn-primary form-control btn-menu" href="cadastrar_produto.php">Cadastrar Produto</a>
                </div>

                <div class="col-sm-12" style="margin-bottom: 10px;">
                  <a class="btn btn-primary form-control btn-menu" href="cadastrar_usuario.php">Cadastrar Usuario</a>
                </div>

                <div class="col-sm-12">
                 <a class="btn btn-primary form-control btn-menu" href="sair.php">Sair</a>
                </div>
              </div>
            </div>              
          </div>
        </div>
        <div class="col-sm-3"></div>
      </div>      
    </div>

    <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>